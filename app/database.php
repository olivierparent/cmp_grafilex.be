<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Maakt connectie met de database.
 * De instellingen worden ingelezen uit het bestand app/config/database.ini
 *
 * @return PDO
 * @throws ErrorException
 */
function maakDatabaseConnectie()
{
    $filename = 'config' . DIRECTORY_SEPARATOR . 'database.ini';
    $config = parse_ini_file($filename); // Zie ook: http://php.net/parse_ini_file

    // DSN (Data Source Name) samenstellen
    $config['dsn'] = $config['dsn.driver'] . ':';
    if (!empty($config['dsn.host'])) {
        $config['dsn'] .= 'host=' . $config['dsn.host'] . ';';
    }
    if (!empty($config['dsn.port'])) {
        $config['dsn'] .= 'port=' . $config['dsn.port'] . ';';
    }
    if (!empty($config['dsn.schema'])) {
        $config['dsn'] .= 'dbname=' . $config['dsn.schema'] . ';';
    }
    if (!empty($config['dsn.charset'])) {
        $config['dsn'] .= 'charset=' . $config['dsn.charset'] . ';';
    }
    try {
        $databaseConnection = new PDO(
             $config['dsn'     ],
             $config['username'],
             $config['password'],
            @$config['options' ]
        );

        $databaseConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $databaseConnection;
    } catch (PDOException $e) {
        throw new ErrorException('Kan geen verbinding maken met (<em>' . $e->getMessage() . '</em>)');
    }
}
