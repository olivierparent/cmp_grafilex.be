<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Begin een nieuwe sessie of ga verder met een bestaande sessie. Dit maakt de globale variabele $_SESSION.
 */
session_start();

/**
 * Verwijdert sessie.
 */
function afmelden()
{
    session_destroy(); // Sessie (en de Browsercookie van de sessie) verwijderen na einde script.
    $_SESSION = [];    // Sessie onmiddellijk leegmaken door een lege array toe te wijzen.
}

/**
 * Is de gebruiker aangemeld?
 *
 * @return bool
 */
function isAangemeld()
{
    return isset($_SESSION['authenticated-grafilex']) ? $_SESSION['authenticated-grafilex'] : false;
}

/**
 * Controleert of de gebruiker aangemeld is, zoniet wordt het script gestopt.
 */
function controleerToegang()
{
    if (!isset($_SESSION['authenticated-grafilex']) || (isset($_SESSION['authenticated-grafilex']) && !$_SESSION['authenticated-grafilex'])) {
        die('Je hebt geen toegang tot deze pagina.');
    }
}
