<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'authentication.php';
require_once $app_dir . 'database.php';
require_once $app_dir . 'utilities.php';

controleerToegang(); // Enkel aangemelde gebruikers mogen toegang hebben tot deze pagina.

if (isset($_FILES['csv-translations']) && !$_FILES['csv-translations']['error']) {
    $bestandsNaam = $_FILES['csv-translations']['tmp_name'];
    $translations = [];

    /**
     * Zie ook: http://php.net/ini_set
     * Zie ook: http://php.net/fopen
     * Zie ook: http://php.net/fgetcsv
     * Zie ook: http://php.net/fclose
     */
    ini_set('auto_detect_line_endings', '1'); // In het geval deze optie in php.ini niet aan zou staan.
    $bestand = fopen($bestandsNaam, 'r');
    if ($bestand) {
        while ($tekstRegel = fgetcsv($bestand, null, ';')) {
            $translations[] = [
                'nl' => $tekstRegel[0],
                'fr' => $tekstRegel[1],
                'en' => $tekstRegel[2],
                'de' => $tekstRegel[3],
            ];
        }
        fclose($bestand);
    }

//    var_dump($translations);

    if (!empty($translations)) {

        $db = maakDatabaseConnectie();

        $languages = [];
        $sql_languages
            = 'SELECT '
            .     'language_id AS id, '
            .     'language_code AS code '
            . 'FROM languages '
            . 'ORDER BY language_code ASC'
        ;

        $res_languages = $db->query($sql_languages);
        if ($res_languages) {
            while ($row_languages = $res_languages->fetch()) {
                $languages[$row_languages['code']] = $row_languages['id'];
            }
        }
//        var_dump($languages); exit;

        $editor_id = $_SESSION['editor']['id'];

        $sql_translations
            = 'INSERT INTO translations ('
            .      'word_id, '
            .      'language_id, '
            .      'translation_value, '
            .      'editor_id'
            . ') VALUES ('
            .      ':word_id, '
            .      ':language_id, '
            .      ':translation_value, '
            .      ':editor_id'
            . ')'
        ;

        /**
         * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
         */
        $stmt_translations = $db->prepare($sql_translations);
        if ($stmt_translations) {
            $stmt_translations->bindValue(':editor_id', $editor_id);

            $sql_words = 'INSERT INTO words () VALUES ()';

            foreach ($translations as $key => $translation) {
                /**
                 * Het eerste item in onze array bestaat uit kolomtitels uit het CSV-bestand. Deze mogen niet in onze
                 * database terechtkomen. $key is array index, en is een getal vanaf 0. 0 wordt door PHP als FALSE
                 * beschouwd, 1 en hoger als TRUE.
                 */
                if ($key) {
                    $db->exec($sql_words);
                    $word_id = $db->lastInsertId();

                    $stmt_translations->bindValue(':word_id', $word_id);

                    foreach ($translation as $language_code => $translation_value) {
                        $language_id = $languages[$language_code];
                        $stmt_translations->bindValue(':language_id', $language_id);
                        $stmt_translations->bindValue(':translation_value', $translation_value);
                        $stmt_translations->execute();
                    }
                }
            }
        }

        $db = null; // Databaseconnectie sluiten.
    }
}

if (isset($_FILES['xml-translations']) && !$_FILES['xml-translations']['error']) {

    $translations = [];

    $pathXml = $_FILES['xml-translations']['tmp_name'];
    $xml = new DOMDocument();
    $xml->load($pathXml);

    $pathXsd = $data_dir . 'vertalingen.xsd';
    if (file_exists($pathXsd)) {
        $isValid = $xml->schemaValidate($pathXsd);
        if ($isValid) {
            $languages = [];
            $sql_languages
                = 'SELECT '
                .     'language_id AS id, '
                .     'language_code AS code '
                . 'FROM languages '
                . 'ORDER BY language_code ASC'
            ;

            $db = maakDatabaseConnectie();

            $res_languages = $db->query($sql_languages);
            if ($res_languages) {
                while ($row_languages = $res_languages->fetch()) {
                    $languages[$row_languages['code']] = $row_languages['id'];
                }
            }
//        var_dump($languages); exit;

            $editor_id = $_SESSION['editor']['id'];

            $sql_translations
                = 'INSERT INTO translations ('
                .      'word_id, '
                .      'language_id, '
                .      'translation_value, '
                .      'editor_id'
                . ') VALUES ('
                .      ':word_id, '
                .      ':language_id, '
                .      ':translation_value, '
                .      ':editor_id'
                . ')'
            ;

            /**
             * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
             */
            $stmt_translations = $db->prepare($sql_translations);
            if ($stmt_translations) {
                $stmt_translations->bindValue(':editor_id', $editor_id);

                $sql_words = 'INSERT INTO words () VALUES ()';

                $translations = new SimpleXMLElement($xml->saveXML());

                foreach ($translations as $translation) {
                    $db->exec($sql_words);
                    $word_id = $db->lastInsertId();

                    $stmt_translations->bindValue(':word_id', $word_id);

                    foreach ($translation->children() as $translation_value) {
                        $language_id = $languages[(string) $translation_value->attributes()['language']];
                        $stmt_translations->bindValue(':language_id', $language_id);
                        $stmt_translations->bindValue(':translation_value', $translation_value);
                        $stmt_translations->execute();
                    }
                }

            }

            $db = null; // Databaseconnectie sluiten.
        }
    }
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Beheer | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Beheer</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Vertalingen</h1>
    <h2 class="col-sm-offset-3 col-sm-9">Importeren</h2>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
        <fieldset>
            <legend>Comma Separated Values</legend>
            <div class="form-group">
                <label for="csv-translations" class="col-sm-3 control-label"><abbr title="Comma Separated Values">CSV</abbr>-bestand:</label>
                <div class="col-sm-9">
                    <input type="file" id="csv-translations" name="csv-translations">
                    <small class="help-block">Importeer een CSV-bestand met UTF-8-encoding.</small>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default" name="btn-import"><i class="glyphicon glyphicon-import"></i> Importeren</button>
            </div>
        </div>
    </form>

    <form action="<?=$_SERVER['REQUEST_URI'] ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
        <fieldset>
            <legend>Extensible Markup Language</legend>
            <div class="form-group">
                <label for="xml-translations" class="col-sm-3 control-label"><abbr title="Extensible Markup Language">XML</abbr>-bestand:</label>
                <div class="col-sm-9">
                    <input type="file" accept="application/xml" id="xml-translations" name="xml-translations">
                    <small class="help-block">Importeer een XML-bestand met UTF-8-encoding.</small>
                </div>
            </div>
        </fieldset>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default" name="btn-import"><i class="glyphicon glyphicon-import"></i> Importeren</button>
            </div>
        </div>
    </form>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
