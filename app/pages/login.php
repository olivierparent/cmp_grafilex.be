<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'utilities.php';

if (isset($_POST['btn-login'])) {
    require_once $app_dir . 'authentication.php'; // Sessie starten
    require_once $app_dir . 'database.php';       // Databasefuncties
    require_once $app_dir . 'security.php';       // Functie verifieerWachtwoord()


    $sql
        = 'SELECT '
        .     'editor_id         AS id, '
        .     'editor_password   AS password, '
        .     'editor_givenname  AS givenname, '
        .     'editor_familyname AS familyname, '
        .     'language_code     AS language '
        . 'FROM editors LEFT JOIN languages USING (language_id) '
        . 'WHERE editor_email = :email '
        . 'LIMIT 1'
    ;

    $db = maakDatabaseConnectie();

    $stmt = $db->prepare($sql);
    if ($stmt) {
        $stmt->bindValue(':email', $_POST['email']);
        $stmt->execute();
        $editor = $stmt->fetch();
        if ($editor) {
            $isAangemeld = verifieerWachtwoord($_POST['password'], $editor['password']);
            if ($isAangemeld) {
                $_SESSION['authenticated-grafilex'] = $isAangemeld;
                $_SESSION['editor'] = [
                    'id'         => $editor['id'],
                    'givenname'  => $editor['givenname'],
                    'familyname' => $editor['familyname'],
                    'language'   => $editor['language'],
                ];
                doorsturenNaar('home');
            } else {
                afmelden();
            }
        }
    }

    $db = null; // Databaseconnectie sluiten.

//    var_dump($_POST);
//    var_dump($sql);
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Aanmelden | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Aanmelden</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Meld je aan</h1>
    <form action="" class="form-horizontal" method="post" autocomplete="off">
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">E-mailadres</label>
            <div class="col-sm-9">
                <input type="email" class="form-control" id="email" name="email" placeholder="Vul je e-mailadres in." required>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Wachtwoord</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="password" name="password" placeholder="Vul je wachtwoord in." required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-login"><i class="glyphicon glyphicon-log-in"></i> Aanmelden</button>
                <a class="btn btn-link" href="index.php?page=home">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
