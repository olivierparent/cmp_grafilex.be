<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'database.php';
require_once $app_dir . 'utilities.php';

// Als formulier gepost is, dan zit de naam van de verzendknop in de global array $_POST.
if (isset($_POST['btn-register'])) {
    require_once $app_dir . 'security.php';

    $sql_editors = 'INSERT INTO editors ('
         .      'editor_email, '
         .      'editor_password, '
         .      'editor_givenname, '
         .      'editor_familyname, '
         .      'editor_gender, '
         .      'editor_birthday, '
         .      'language_id'
         . ') VALUES ('
         .      ':email, '
         .      ':password, '
         .      ':givenname, '
         .      ':familyname, '
         .      ':gender, '
         .      ':birthday, '
         .      ':language'
         . ')'
    ;

    $db = maakDatabaseConnectie();

    $stmt_editors = $db->prepare($sql_editors);
    if ($stmt_editors) {
        $stmt_editors->bindValue(':email'     , $_POST['email']);
        $stmt_editors->bindValue(':password'  , hashWachtwoord($_POST['password']));
        $stmt_editors->bindValue(':givenname' , $_POST['givenname']);
        $stmt_editors->bindValue(':familyname', $_POST['familyname']);
        $stmt_editors->bindValue(':gender'    , $_POST['gender']);
        $stmt_editors->bindValue(':birthday'  , $_POST['birthday']);
        $stmt_editors->bindValue(':language'  , $_POST['language']);
        $stmt_editors->execute();
    }

    $db = null; // Databaseconnectie sluiten.

    doorsturenNaar('home');
}

$db = maakDatabaseConnectie();

$sql_languages
    = 'SELECT '
    .     'language_id AS id, '
    .     'language_description AS description '
    . 'FROM languages '
    . 'ORDER BY language_description ASC'
;

$res_languages = $db->query($sql_languages);
if ($res_languages) {
    $languages = $res_languages->fetchAll();
}

$db = null; // Databaseconnectie sluiten.

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Registreren | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Registreren</li>
    </ol>
<?php if (!empty($_POST)) : ?>
    <div class="well">
        <h2>Kan je niet registreren</h2>
<?php
    $voornaam = $_POST['givenname'];
    if (strlen($voornaam) <= 0) {
        echo '<p>Voornaam niet ingevuld.</p>', PHP_EOL;
    }

    $familienaam = $_POST['familyname'];
    if (strlen($familienaam) <= 0) {
        echo '<p>Familienaam niet ingevuld.</p>', PHP_EOL;
    }
?>
    </div>
<?php endif ?>
    <h1 class="col-sm-offset-3 col-sm-9">Registreer je</h1>
    <form action="" class="form-horizontal" method="post" role="form" autocomplete="off">
        <fieldset>
            <legend>Aanmeldgegevens</legend>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">E-mailadres</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Vul je e-mailadres in." required>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Wachtwoord</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Vul je wachtwoord in." required>
                </div>
            </div>
            <div class="form-group">
                <label for="password-repeat" class="col-sm-3 control-label">Wachtwoord herhalen</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Herhaal je wachtwoord." required>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Personalia</legend>
            <div class="form-group">
                <label for="givenname" class="col-sm-3 control-label">Voornaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="givenname" name="givenname" placeholder="Vul je voornaam in." value="<?=isset($voornaam) ? $voornaam : '' ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="familyname" class="col-sm-3 control-label">Familienaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="familyname" name="familyname" placeholder="Vul je familienaam in." value="<?=isset($familienaam) ? $familienaam : '' ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Geslacht</label>
                <div class="col-sm-9">
                    <div class="radio-inline">
                        <input type="radio" id="gender-male" name="gender" value="male" required>
                        <label for="gender-male">Man</label>
                    </div>
                    <div class="radio-inline">
                        <input type="radio" id="gender-female" name="gender" value="female" required>
                        <label for="gender-female">Vrouw</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="birthday" class="col-sm-3 control-label">Geboortedatum</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="birthday" name="birthday" required>
                </div>
            </div>
            <div class="form-group">
                <label for="language" class="col-sm-3 control-label">Taal</label>
                <div class="col-sm-9">
                    <select class="form-control" name="language" id="language">
                        <option value="" selected>&mdash; Kies je taal &mdash;</option>
                        <?php foreach ($languages as $language): ?>
                            <option value="<?=$language['id'] ?>"><?=$language['description'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-register">Registreren</button>
                <a class="btn btn-link" href="index.php?page=home">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
