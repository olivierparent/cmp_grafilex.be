<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'authentication.php'; // Sessie starten
require_once $app_dir . 'database.php';
require_once $app_dir . 'navigation.php';
require_once $app_dir . 'utilities.php';      // voor functie copyDate() in footer pagina

$isAangemeld = isAangemeld();

$menu = readMenuJson();

$sql_translations
    = 'SELECT * FROM '
    . '(SELECT word_id AS id, translation_value AS en FROM words NATURAL LEFT JOIN (SELECT * FROM translations WHERE language_id = 1) AS L1) AS T1 '
    .     'NATURAL JOIN '
    . '(SELECT word_id AS id, translation_value AS de FROM words NATURAL LEFT JOIN (SELECT * FROM translations WHERE language_id = 2) AS L2) AS T2 '
    .     'NATURAL JOIN '
    . '(SELECT word_id AS id, translation_value AS fr FROM words NATURAL LEFT JOIN (SELECT * FROM translations WHERE language_id = 3) AS L3) AS T3 '
    .     'NATURAL JOIN '
    . '(SELECT word_id AS id, translation_value AS nl FROM words NATURAL LEFT JOIN (SELECT * FROM translations WHERE language_id = 4) AS L4) AS T4 '
    . 'ORDER BY nl ASC'
;

$db = maakDatabaseConnectie();

$translations = [];

$res_translations = $db->query($sql_translations);
if ($res_translations) {
    $translations = $res_translations->fetchAll();
}

$db = null; // Databaseconnectie sluiten.

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Woordenlijst | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<header>
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-grafilex">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?page=home" title="Start">Grafilex.be</a>
        </div>
        <div id="navbar-grafilex" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
<?php foreach ($menu as $key => $menuItem): ?>
<?php $menuItemActive = (isset($_GET['page']) && $_GET['page'] === $menuItem->link) ? ' class="active"' : '' ?>
                <li<?=$menuItemActive ?>><a href="?page=<?=$menuItem->link ?>"><?=$menuItem->label ?></a></li>
<?php endforeach ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
<?php if ($isAangemeld): ?>
                <li><a href="?page=import" class="navbar-link"><i class="glyphicon glyphicon-wrench"></i> Beheer</a></li>
                <li><a href="?page=logout" class="navbar-link"><i class="glyphicon glyphicon-log-out"></i> Afmelden</a></li>
<?php else: ?>
                <li><a href="?page=register" class="navbar-link"><i class="glyphicon glyphicon-user"></i> Registreren</a></li>
                <li><a href="?page=login" class="navbar-link"><i class="glyphicon glyphicon-log-in"></i> Aanmelden</a></li>
<?php endif ?>
            </ul>
        </div>
    </nav>
    <div class="hidden-xs hidden-lg">
        <img src="http://lorempixel.com/1920/200/abstract/7" alt="" class="img-responsive">
    </div>
</header>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Woordenlijst</li>
    </ol>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Nederlands</th>
                <th scope="col">Engels</th>
                <th scope="col">Frans</th>
                <th scope="col">Duits</th>
<?php if ($isAangemeld): ?>
                <th scope="col" class="text-center"><i class="glyphicon glyphicon-edit" title="Vertalingen bewerken"></i></th>
                <th scope="col" class="text-center"><i class="glyphicon glyphicon-remove" title="Vertalingen verwijderen"></i></th>
<?php endif ?>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Nederlands</th>
                <th scope="col">Engels</th>
                <th scope="col">Frans</th>
                <th scope="col">Duits</th>
<?php if ($isAangemeld): ?>
                <th scope="col" class="text-center"><i class="glyphicon glyphicon-edit" title="Vertalingen bewerken"></i></th>
                <th scope="col" class="text-center"><i class="glyphicon glyphicon-remove" title="Vertalingen verwijderen"></i></th>
<?php endif ?>
            </tr>
        </tfoot>
        <tbody>
<?php $i = 1; foreach ($translations as $translation): ?>
            <tr>
                <th scope="row" class="text-right"><?=$i++ ?></th>
<?php if ($isAangemeld): ?>
                <td><?php if (empty($translation['nl'])): ?><a href="index.php?page=vocabulary_edit&update_word=<?=$translation['id'] ?>" class="btn btn-xs btn-default" title="Vertaling toevoegen"><i class="glyphicon glyphicon-question-sign"></i></a><?php else: ?><?=$translation['nl'] ?><?php endif ?></td>
                <td><?php if (empty($translation['en'])): ?><a href="index.php?page=vocabulary_edit&update_word=<?=$translation['id'] ?>" class="btn btn-xs btn-default" title="Vertaling toevoegen"><i class="glyphicon glyphicon-question-sign"></i></a><?php else: ?><?=$translation['en'] ?><?php endif ?></td>
                <td><?php if (empty($translation['fr'])): ?><a href="index.php?page=vocabulary_edit&update_word=<?=$translation['id'] ?>" class="btn btn-xs btn-default" title="Vertaling toevoegen"><i class="glyphicon glyphicon-question-sign"></i></a><?php else: ?><?=$translation['fr'] ?><?php endif ?></td>
                <td><?php if (empty($translation['de'])): ?><a href="index.php?page=vocabulary_edit&update_word=<?=$translation['id'] ?>" class="btn btn-xs btn-default" title="Vertaling toevoegen"><i class="glyphicon glyphicon-question-sign"></i></a><?php else: ?><?=$translation['de'] ?><?php endif ?></td>
                <td><a href="index.php?page=vocabulary_edit&update_word=<?=$translation['id'] ?>" class="btn btn-xs btn-primary" title="Vertalingen bewerken"><i class="glyphicon glyphicon-edit"></i></a></td>
                <td><a href="index.php?page=vocabulary_edit&delete_word=<?=$translation['id'] ?>" class="btn btn-xs btn-danger" title="Vertalingen verwijderen"><i class="glyphicon glyphicon-remove"></i></a></td>
<?php else: ?>
                <td><?php if (empty($translation['nl'])): ?><i class="glyphicon glyphicon-question-sign"></i><?php else: ?><?=$translation['nl'] ?><?php endif ?></td>
                <td><?php if (empty($translation['en'])): ?><i class="glyphicon glyphicon-question-sign"></i><?php else: ?><?=$translation['en'] ?><?php endif ?></td>
                <td><?php if (empty($translation['fr'])): ?><i class="glyphicon glyphicon-question-sign"></i><?php else: ?><?=$translation['fr'] ?><?php endif ?></td>
                <td><?php if (empty($translation['de'])): ?><i class="glyphicon glyphicon-question-sign"></i><?php else: ?><?=$translation['de'] ?><?php endif ?></td>
<?php endif ?>
            </tr>
<?php endforeach ?>
        </tbody>
    </table>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
