<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'authentication.php';
require_once $app_dir . 'database.php';
require_once $app_dir . 'utilities.php';

controleerToegang(); // Enkel aangemelde gebruikers mogen toegang hebben tot deze pagina.

$translations = [];

if (isset($_GET['delete_word'])) {

    /**
     * Foreign Key in translations moet ON DELETE CASCADE hebben, anders kan de rij niet verwijderd worden.
     * Ook alle rijen met die FK worden verwijderd!
     */
    $sql_words
        = 'DELETE FROM words '
        . 'WHERE word_id = :word_id'
    ;

    $db = maakDatabaseConnectie(); // Databaseconnectie openen.

    $stmt_words = $db->prepare($sql_words);
    if ($stmt_words) {
        $stmt_words->bindValue(':word_id', $_GET['delete_word']);
        $stmt_words->execute();
    }

    $db = null; // Databaseconnectie sluiten.

    doorsturenNaar('vocabulary');
}

if (isset($_GET['update_word'])) {
    $languages = [];

    $word_id = $_GET['update_word'];

    $db = maakDatabaseConnectie();

    $sql_languages
        = 'SELECT '
        .     'language_id          AS id, '
        .     'language_code        AS code, '
        .     'language_description AS description '
        . 'FROM languages '
        . 'ORDER BY '
        .     'CASE language_code WHEN :language_code THEN 1 ELSE 2 END ASC, ' // Taal gebruiker komt eerst in sortering.
        .     'language_code ASC'                                              // Rest talen alfabetisch.
    ;

    $stmt_languages = $db->prepare($sql_languages);
    if ($stmt_languages) {
        $language_code = $_SESSION['editor']['language'];
        $stmt_languages->bindValue(':language_code', $language_code);
        if ($stmt_languages->execute()) {
            while ($row_languages = $stmt_languages->fetch()) {
                $languages[$row_languages['code']] = [
                    'id'          => $row_languages['id'],
                    'description' => $row_languages['description'],
                ];
            }
        }
    }

    if (isset($_POST) && isset($_POST['btn-update'])) {

//        var_dump($_POST);

        /**
         * Haal een lijst op van alle talen waarin er een vertaling voor het woord beschikbaar is.
         */
        $translationsAvailable = [];
        $sql_translations_available
            = 'SELECT '
            .     'language_code AS language '
            . 'FROM translations LEFT JOIN languages '
            .     'USING (language_id) '
            . 'WHERE word_id = :word_id';
        ;
        $stmt_translations_available = $db->prepare($sql_translations_available);
        if ($stmt_translations_available) {
            $stmt_translations_available->bindValue(':word_id', $word_id);
            $stmt_translations_available->execute();
            while ($row = $stmt_translations_available->fetch()) {
                $translationsAvailable[] = $row['language'];
            }
        }

        $sql_translations_update
            = 'UPDATE translations '
            . 'SET '
            .     'translation_value = :translation_value, '
            .     'translation_updated = CURRENT_TIMESTAMP, '
            .     'editor_id = :editor_id '
            . 'WHERE '
            .     'word_id = :word_id AND '
            .     'language_id = :language_id'
        ;

        $sql_translations_insert
            = 'INSERT INTO translations ('
            .      'word_id, '
            .      'language_id, '
            .      'translation_value, '
            .      'editor_id'
            . ') VALUES ('
            .      ':word_id, '
            .      ':language_id, '
            .      ':translation_value, '
            .      ':editor_id'
            . ')'
        ;


        $stmt_translations_update = $db->prepare($sql_translations_update);
        $stmt_translations_insert = $db->prepare($sql_translations_insert);

        if ($stmt_translations_update && $stmt_translations_insert) {
            $stmt_translations_update->bindValue(':word_id', $word_id);
            $stmt_translations_insert->bindValue(':word_id', $word_id);

            $editor_id = $_SESSION['editor']['id'];
            $stmt_translations_update->bindValue(':editor_id', $editor_id);
            $stmt_translations_insert->bindValue(':editor_id', $editor_id);

            if (is_array($_POST['btn-update'])) {
                /**
                 * Zie ook: http://php.net/key
                 */
                $language          = key($_POST['btn-update']);
                $translation_value = $_POST['translation'][$language];

                $language_id = $languages[$language]['id'];
                if (in_array($language, $translationsAvailable)) {
                    // UPDATE translations SET ... WHERE ...
                    $stmt_translations_update->bindValue(':translation_value', $translation_value);
                    $stmt_translations_update->bindValue(':language_id', $language_id);
                    $stmt_translations_update->execute();
                } else {
                    // INSERT INTO translations (...) VALUES (...)
                    $stmt_translations_insert->bindValue(':translation_value', $translation_value);
                    $stmt_translations_insert->bindValue(':language_id', $language_id);
                    $stmt_translations_insert->execute();
                }
            } else {
                foreach($_POST['translation'] as $language => $translation_value) {
                    $language_id = $languages[$language]['id'];
                    if (in_array($language, $translationsAvailable)) {
                        // UPDATE translations SET ... WHERE ...
                        $stmt_translations_update->bindValue(':translation_value', $translation_value);
                        $stmt_translations_update->bindValue(':language_id', $language_id);
                        $stmt_translations_update->execute();
                    } else {
                        // INSERT INTO translations (...) VALUES (...)
                        $stmt_translations_insert->bindValue(':translation_value', $translation_value);
                        $stmt_translations_insert->bindValue(':language_id', $language_id);
                        $stmt_translations_insert->execute();
                    }
                }

            }
        }
    }

    if (isset($_POST) && isset($_POST['btn-delete'])) {

        $sql_translations_delete
            = 'DELETE FROM translations '
            . 'WHERE '
            .     'word_id = :word_id AND '
            .     'language_id = :language_id'
        ;

        $stmt_translations_delete = $db->prepare($sql_translations_delete);

        if ($stmt_translations_delete) {
            $language = key($_POST['btn-delete']);
            $language_id = $languages[$language]['id'];

            $stmt_translations_delete->bindValue(':word_id', $word_id);
            $stmt_translations_delete->bindValue(':language_id', $language_id);
            $stmt_translations_delete->execute();
        }
    }

    $sql_translations
        = 'SELECT '
        .     'language_code AS code, '
        .     'translation_value AS value '
        . 'FROM translations NATURAL JOIN languages '
        . 'WHERE word_id = :word_id'
    ;

    $stmt_translations = $db->prepare($sql_translations);
    if ($stmt_translations) {

        $stmt_translations->bindValue(':word_id', $word_id);

        if ($stmt_translations->execute()) {
            while ($row = $stmt_translations->fetch()) {
                $translations[$row['code']] = $row['value'];
            }
        }
    }

    $db = null; // Databaseconnectie sluiten.
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Woordenlijst | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li><a href="?page=vocabulary">Woordenlijst</a></li>
        <li class="active">Edit</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Vertalingen</h1>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post">
        <fieldset>
            <legend>Woord <span class="badge"><?=$_GET['update_word'] ?></span></legend>
<?php foreach ($languages as $languageCode => $language): ?>
            <div class="form-group">
                <label for="translation-<?=$languageCode ?>" class="col-sm-3 control-label"><?=$language['description'] ?></label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" name="btn-update[<?=$languageCode ?>]"><i class="glyphicon glyphicon-save"></i> Bewaar</button>
                        </span>
                        <input type="text" class="form-control" id="translation-<?=$languageCode ?>" name="translation[<?=$languageCode ?>]" placeholder="Voeg een vertaling toe naar het <?=$language['description'] ?>." value="<?=isset($translations[$languageCode]) ? $translations[$languageCode] : '' ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="submit" name="btn-delete[<?=$languageCode ?>]">Verwijder <i class="glyphicon glyphicon-remove"></i></button>
                        </span>
                    </div>
                </div>
            </div>
<?php endforeach ?>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button class="btn btn-primary" type="submit" name="btn-update"><i class="glyphicon glyphicon-save"></i> Bewaar alles</button>
                <a href="?page=vocabulary_edit&delete_word=<?=$_GET['update_word'] ?>" class="btn btn-danger pull-right" name="btn-delete">Verwijder alles <i class="glyphicon glyphicon-remove"></i></a>
            </div>
        </div>
    </form>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
