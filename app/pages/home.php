<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'authentication.php'; // Sessie starten.
require_once $app_dir . 'navigation.php';
require_once $app_dir . 'utilities.php';      // voor functie copyDate() in footer pagina

$isAangemeld = isAangemeld();

$menu = readMenuJson();

$welkom = '';
if ($isAangemeld) {
    $persoon = $_SESSION['editor'];
    welkomBoodschap($welkom, $persoon);
} else {
    welkomBoodschap($welkom);
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<header>
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-grafilex">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?page=home" title="Start">Grafilex.be</a>
        </div>
        <div id="navbar-grafilex" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
<?php foreach ($menu as $key => $menuItem): ?>
<?php $menuItemActive = (isset($_GET['page']) && $_GET['page'] === $menuItem->link) ? ' class="active"' : '' ?>
                <li<?=$menuItemActive ?>><a href="?page=<?=$menuItem->link ?>"><?=$menuItem->label ?></a></li>
<?php endforeach ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
<?php if ($isAangemeld): ?>
                <li><a href="?page=import" class="navbar-link"><i class="glyphicon glyphicon-wrench"></i> Beheer</a></li>
                <li><a href="?page=logout" class="navbar-link"><i class="glyphicon glyphicon-log-out"></i> Afmelden</a></li>
<?php else: ?>
                <li><a href="?page=register" class="navbar-link"><i class="glyphicon glyphicon-user"></i> Registreren</a></li>
                <li><a href="?page=login" class="navbar-link"><i class="glyphicon glyphicon-log-in"></i> Aanmelden</a></li>
<?php endif ?>
            </ul>
        </div>
    </nav>
    <div class="hidden-xs hidden-lg">
        <img src="http://lorempixel.com/1920/200/abstract/7" alt="" class="img-responsive">
    </div>
</header>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Startpagina</li>
    </ol>
    <div class="jumbotron">
        <h1><?=$welkom ?></h1>
        <p>Det grafisch lexicon is een demoproject voor het <abbr title="Opleingsonderdeel">OLOD</abbr> <strong>Crossmedia Publishing</strong> van de afstudeerrichtingen
            <abbr title="Grafimediabeleid">GBM</abbr> en <abbr title="Grafimediatechnologie">GMT</abbr> van de opleiding <strong>Bachelor in de Grafische en digitale media</strong> aan de Arteveldehogeschool.</p>
        <p>In dit project wordt de basis van PHP 5.4 en MySQL gedemonstreerd aan de hand van niet-<abbr title="Objectgeoriënteerde">OO</abbr> code.</p>
    </div>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
