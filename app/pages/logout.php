<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once $app_dir . 'utilities.php';

if (isset($_POST['btn-logout'])) {
    require_once $app_dir . 'authentication.php'; // Sessie starten.

    afmelden();
    doorsturenNaar('home');
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Afmelden | Grafilex.be</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/<?=$config['library']['Font-Awesome'] ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="styles/default.css">
</head>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Afmelden</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Wil je je afmelden?</h1>
    <p class="col-sm-offset-3 col-sm-9">Klik op de knop om je af te melden.</p>
    <form action="" class="form-horizontal" method="post">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-logout"><i class="glyphicon glyphicon-log-out"></i> Afmelden</button>
                <a class="btn btn-link" href="index.php?page=home">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<footer id="footer">
    <p><?=copyDate() ?> <a href="#">Grafilex.be</a>, Graphic Lexicon by <a href="http://www.arteveldehogeschool.be/">Artevelde University College Ghent</a></p>
</footer>
<!-- SCRIPTS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/<?=$config['library']['jQuery'] ?>/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/<?=$config['library']['Bootstrap'] ?>/js/bootstrap.min.js"></script>
</body>
</html>
