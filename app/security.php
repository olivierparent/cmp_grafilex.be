<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Berekent de hashcode ('vingerafdruk') voor het wachtwoord.
 *
 * @param $wachtwoord
 * @return string gehastWachtwoord Hashcode voor het wachtwoord.
 */
function hashWachtwoord($wachtwoord)
{
    $algo = '2y'; // Code voor het Blowfish-algoritme dat gebruikt zal worden
    $cost = 15;   // Hoe hoger de cost, hoe langer het duurt om de hashcode te berekenen.
    $randomSalt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

    $salt = '$' . $algo . '$' . $cost . '$' . $randomSalt;

    $gehashtWachtwoord = crypt($wachtwoord, $salt);

    return $gehashtWachtwoord;
}

/**
 * Verifieer het wachtwoord door het opnieuw te hashen met het hashcode  als salt.
 * Als het wachtwoord correct is, dan moet de hashcode exact hetzelfde zijn als de hashcode die
 * als salt gebruikt werd.
 *
 * @param $wachtwoord
 * @param $gehashtWachtwoord
 * @return bool
 */
function verifieerWachtwoord($wachtwoord, $gehashtWachtwoord)
{
    $salt = $gehashtWachtwoord;
    $gehashtWachtwoord2 = crypt($wachtwoord, $salt);

    if ($gehashtWachtwoord2 === $gehashtWachtwoord) {
        return true;
    } else {
        return false;
    }
}