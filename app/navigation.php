<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . 'utilities.php';

/**
 * @return bool|SimpleXMLElement
 */
function readMenuXml()
{
    $path = __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

    $pathXml = $path . 'navigation.xml';
    $pathXsd = $path . 'navigation.xsd';
    $pathXsl = $path . 'navigation.xsl';

    if (file_exists($pathXml)) {
        $xml = new DOMDocument();
        $xml->load($pathXml);

        if(file_exists($pathXsd) && file_exists($pathXsl)) {
            $valid = $xml->schemaValidate($pathXsd);
            if($valid) {
                $xsl = new DOMDocument();
                $xsl->load($pathXsl);

                /**
                 * BitNami WAMP Stack: zorg ervoor dat de XSL extensie aan staat in
                 * C:\BitNami\wampstack-5.4.20-0\php\php.ini:
                 * extension=php_xsl.dll
                 */
                $xp = new XSLTProcessor();
                $xp->importStylesheet($xsl);
                $xmlSorted = new SimpleXMLElement($xp->transformToXML($xml));

                return $xmlSorted;
            }
        }
        return readXml($path . 'navigation.xml');
    }

    die('Bestand ' . $pathXml . ' niet gevonden.');
}

/**
 * @return bool|SimpleXMLElement
 */
function readMenuJson()
{
    $path = __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

    $pathJson = $path . 'navigation.json';

    if (file_exists($pathJson)) {
        $json = file_get_contents($pathJson);
        $json = json_decode($json);

        return $json->navigation;
    }

    die('Bestand ' . $pathJson . ' niet gevonden.');
}
