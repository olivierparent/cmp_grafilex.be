<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Toont een string met daarin de copyright boodschap.
 *
 * @return string
 */
function copyDate()
{
    global $config; // gebruikt NOOIT global variabelen, omdat je nooit weet wat er allemaal mee gebeurd is voordat ze hier gebruikt worden.

    $startDate   = $config['startDate'];
    $currentDate = (int) date('Y'); // date() geeft een string terug met het huidig jaartal, maar we willen een integer.

    return 'Copyright &copy; ' . ($startDate < $currentDate ? "{$startDate}-{$currentDate}" : $currentDate); // Ternaire operator: ( conditie ? als waar : als onwaar)
}

/**
 * Vervangt de inhoud van de variabele met een welkomstboodschap.
 *
 * Zie ook: http://php.net/implode
 * Zie ook: http://php.net/ucwords
 *
 * @param $welkom Deze parameter is pass-by-reference (&): de originele variable die als argument meegegeven wordt zal gewijzigd worden in plaats van er eerst een kopie van te maken.
 * @param array $persoon
 */
function welkomBoodschap(&$welkom, array $persoon = [])
{
    $welkom = ['Welkom'];

    if (empty($persoon)) {
        $welkom[] = 'bezoeker';
    } else {
        if (isset($persoon['givenname'])) {
            $welkom[] = ucwords($persoon['givenname']);
        }
        if (isset($persoon['familyname'])) {
            $welkom[] = ucwords($persoon['familyname']);
        }
    }

    $welkom = implode(' ', $welkom) . '!';
}

/**
 * @param $path
 * @return bool|SimpleXMLElement
 */
function readXml($path)
{
    if (file_exists($path)) {
        return $xml = simplexml_load_file($path);
    }

    return false;
}

/**
 * Stuurt de browser naar een andere pagina. Let er op dat er vooraf geen output naar de browser gestuurd wordt, anders
 * is de HTTP-header al afgesloten, waardoor je er niets meer in kan veranderen.
 *
 * @param string $pagina
 */
function doorsturenNaar($pagina)
{
    $uri = $_SERVER['HTTP_ORIGIN'] . $_SERVER['SCRIPT_NAME'] . '?page=' . $pagina;
    header("Location: {$uri}");
}
