	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *                        aaaAAaaa            HHHHHH                         *
	 *                     aaAAAAAAAAAAaa         HHHHHH                         *
	 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
	 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
	 *                   aAAAAAa    aAAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   aAAAAAa     AAAAAA                                      *
	 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
	 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
	 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
	 *                         aaAAAAAAAAAA       HHHHHH                         *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
	 *                                                                           *
	 *                                                                           *
	 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
	 *                                                                           *
	 *                                                                           *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	 Olivier Parent
	 Copyright © 2013 Artevelde University College Ghent


Crossmedia Publishing - Grafilex.be
===================================
Grafisch Lexicon

Installatieinstructies
----------------------

    $ git clone https://bitbucket.org/olivierparent/cmp_grafilex.be CMP_grafilex.be
    $ cd CMP_grafilex.be

### Database

Maak een database aan, zie: docs/CMP_database.mwb

Pas de configuratie aan, zie: `app/config/database.ini`

### Homepagina

De onderstaande URL's zijn voor BitNami MAMP/WAMP Stack

Mac OS X (server manueel starten!):

    http://localhost:8080/CMP_grafilex.be/web/
    http://localhost:8080/CMP_grafilex.be/web/?page=home

Windows

    http://localhost/CMP_grafilex.be/web/
    http://localhost/CMP_grafilex.be/web/?page=home
