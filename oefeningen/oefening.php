§<?php
/**
 * Datatypes in PHP;
 */

// integer (geheel getal)
echo '<h1>integer</h1>';
$i = 1;
echo '<code>$i = 1;</code>';
var_dump($i);

echo '<br>';

$i = $i + 1;
echo '<code>$i = $i + 1;</code>';
var_dump($i);

echo '<br>';

echo '<code>$i++;</code> (post-increment operator)';
var_dump($i++);
var_dump($i);

echo '<br>';

echo '<code>++$i;</code> (pre-increment operator)';
var_dump(++$i);
var_dump($i);

echo '<br>';

echo '<code>$i--;</code> (post-decrement operator)';
var_dump($i--);
var_dump($i);

echo '<br>';

echo '<code>--$i;</code> (pre-decrement operator)';
var_dump(--$i);
var_dump($i);

echo '<hr>';

// float (vlottende komma getal)
echo '<h1>float</h1>';
$f = 1.1;
echo '<code>$f = 1.1;</code>';
var_dump($f);

echo '<code>$f = $f + .2;</code>';
$f = $f + .2;
var_dump($f);

echo '<hr>';

// boolean
echo '<h1>boolean</h1>';

$b = true;
echo '<code>$b = true;</code>';
var_dump($b);

echo '<br>';

$b = false;
echo '<code>$b = false;</code>';
var_dump($b);

echo '<h2>Booleaanse algebra</h2>';

echo '<h3>Logische EN (<code>&&</code> operator)</h3>';

echo '<code>true && true</code>';
var_dump(true && true);

echo '<br>';

echo '<code>false && true</code>';
var_dump(false && true);

echo '<br>';

echo '<code>true && false</code>';
var_dump(true && false);

echo '<br>';

echo '<code>false && false</code>';
var_dump(false && false);

echo '<h3>Logische OF (<code>||</code> operator)</h3>';

echo '<code>true || true</code>';
var_dump(true || true);

echo '<br>';

echo '<code>false || true</code>';
var_dump(false || true);

echo '<br>';

echo '<code>true || false</code>';
var_dump(true || false);

echo '<br>';

echo '<code>false || false</code>';
var_dump(false || false);

echo '<hr>';

echo '<h1>Array</h1>';

// Array
$a = [];
echo '<code>$a = [];</code> (lege Array)';
var_dump($a);

echo '<br>';

$a = [];
echo '<code>$a = [];</code> (lege Array)<br>';
echo '<code>$a[] = \'one\';</code><br>';
echo '<code>$a[] = \'two\';</code><br>';
echo '<code>$a[] = \'three\';</code>';
$a[] = 'one';
$a[] = 'two';
$a[] = 'three';
var_dump($a);

echo '<br>';

$a = ['één', 'twee', 'drie'];
echo '<code>$a = [\'één\', \'twee\', \'drie\'];</code>';
var_dump($a);

echo '<br>';

$a = [0 => 'één', 1 => 'twee',  2 => 'drie'];
echo '<code>$a = [0 => \'één\', 1 => \'twee\', 2 => \'drie\'];</code>';
var_dump($a);

echo '<br>';

$a = ['one' => 'één', 'two' => 'twee',  'three' => 'drie'];
echo '<code>$a = [\'one\' => \'één\', \'two\' => \'twee\', \'three\' => \'drie\'];</code>';
var_dump($a);

echo '<br>';

echo '<code>$a[\'one\'];</code>';
var_dump($a['one']);

echo '<br>';

echo '<code>$a[\'two\'];</code>';
var_dump($a['two']);

echo '<br>';

echo '<code>$a[\'three\'];</code>';
var_dump($a['three']);

echo '<h2>Multidimensionele Array (Array\'s in Array\'s)</h2>';

$a = ['one' => ['één', 'un'], 'two' => ['twee', 'deux'],  'three' => ['drie', 'trois']];
echo '<code>$a = [\'one\' => [\'één\', \'un\'], \'two\' => [\'twee\', \'deux\'],  \'three\' => [\'drie\', \'trois\']];</code>';
var_dump($a);

echo '<code>$a[\'three\'];</code>';
var_dump($a['three']);

echo '<code>$a[\'three\'][1];</code>';
var_dump($a['three'][1]);

echo '<br>';

$a = ['one' => ['nl' => 'één', 'fr' => 'un'], 'two' => ['nl' => 'twee', 'fr' => 'deux'],  'three' => ['nl' => 'drie', 'fr' => 'trois']];
echo '<code>$a = [\'nl\' => \'one\' => [\'één\', \'fr\' => \'un\'], \'two\' => [\'nl\' => \'twee\', \'fr\' => \'deux\'],  \'three\' => [\'nl\' => \'fr\' => \'drie\', \'fr\' => \'trois\']];</code>';
var_dump($a);

echo '<code>$a[\'three\'];</code>';
var_dump($a['three']);

echo '<code>$a[\'three\'][\'fr\'];</code>';
var_dump($a['three']['fr']);

echo '<hr>';

$s = '1';
var_dump($s);
// Output: string '1' (length=1)

$i = (int) $s;
//$i = (integer) $s;
var_dump($i);
// Output: int 1

$f = (float) $s;
//$f = (double) $s;
//$f = (real) $s;
var_dump($f);
// Output: float 1

$b = (boolean) $s;
//$b = (bool) $s;
var_dump($b);
// Output: boolean true

$a = (array)  $s;
var_dump($a);
/* Output:
 * array (size=1)
 *   0 => string '1' (length=1)
 */

$o = (object) $s;
var_dump($o);
/* Output:
 * object(stdClass)[1]
 *   public 'scalar' => string '1' (length=1)
 */

$u = (unset) $s;
var_dump($u);
// Output: null

echo '<hr>';

$i1 = 1;
$d1 = 1.1;
$s1 = 'string1';   // int 0
$s2 = '1string';   // int 1
$s3 = '1.1string'; // float 1.1
$b1 = true;        // int 1
$a1 = [];
$a2 = [1];
$a3 = ['a','b'];

var_dump($i1 + $s1);
// Output: int 1

var_dump($i1 + $d1);
// Output: float 2.1

var_dump($i1 + $s2);
// Output: int 2

var_dump($i1 + $s3);
// Output: float 2.1

var_dump($s2 + $i1);
// Output: int 2

var_dump($s2 + $s3);
// Output: float 2.1

var_dump($i1 + $b1 + $d1);
// Output: float 3.1

var_dump($a1 + $a2 + $a3);
/* Output:
 * array (size=2)
 *   0 => int 1
 *   1 => string 'b' (length=1)
 */