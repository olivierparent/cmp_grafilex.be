<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * bepaal de map waarin de applicatie staat, relatief ten opzichte van dit bestand (index.php).
 */
$app_dir
    = __DIR__             // pad naar dit bestand
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
    . '..'                // parent-folder
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
    . 'app'               // 'app' folder
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
;
$data_dir
    = __DIR__             // pad naar dit bestand
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
    . '..'                // parent-folder
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
    . 'data'               // 'app' folder
    . DIRECTORY_SEPARATOR // '\' op Windows, '/' op Mac OS X
;

/**
 * Lees het configuratiestand in. Vang de array op in de variable $config.
 */
$config = require_once $app_dir . 'config' . DIRECTORY_SEPARATOR . 'config.php';

/**
 * @param $pageName
 * @return string Volledig pad naar het PHP-bestand voor de pagina.
 */
function pagePath($pageName)
{
    global $app_dir;

    return $app_dir . 'pages' . DIRECTORY_SEPARATOR . $pageName . '.php';
}

/**
 * Paginanaam uit de GET-variabele met de naam 'page' halen (URL: ?page=paginanaam)
 * Indien er een GET-variabele met de naam 'page' is, gebruik de waarde, zoniet gebruik 'home'.
 */
$pageName = isset($_GET['page']) ? $_GET['page'] : 'home';
$page = pagePath($pageName);
if (file_exists($page)) {
    require $page;
} else {
    $error = [
        'title'   => 'Fout 404',
        'message' => "De pagina '{$_GET['page']}' kon niet gevonden worden.",
    ];

    header('HTTP/1.0 404 Not Found');

    require pagePath('error');
}
