<?php

$mijnHashcode = '$2y$15$aXgVHT0ng/7ynDSM92GnduFDJfMMOXYF7ZdrDLv3hY4q.WgesAJDO';

function hashWachtwoord($wachtwoord)
{
    $algo = '2y'; // Blowfish algoritme
    $cost = 15;
    $randomSalt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

    $salt = '$' . $algo . '$' . $cost . '$' . $randomSalt;

    echo '<h3><code>$randomSalt</code></h3>';
    var_dump($randomSalt);
    echo '<h3><code>$salt</code></h3>';
    var_dump($salt);

    $gehashtWachtwoord = crypt($wachtwoord, $salt);

    return $gehashtWachtwoord;
}

function verifieerWachtwoord($wachtwoord, $gehashtWachtwoord)
{
    return crypt($wachtwoord, $gehashtWachtwoord) === $gehashtWachtwoord;
}

if (isset($_POST['btn-register'])) {
//    var_dump($_POST);

    $wachtwoord = $_POST['wachtwoord'];
    echo '<h3><code>$wachtwoord</code></h3>';
    var_dump($wachtwoord);

    $gehashtWachtwoord = hashWachtwoord($wachtwoord);
    echo '<h3><code>$gehashtWachtwoord</code></h3>';
    var_dump($gehashtWachtwoord);

    echo '<h3><code>crypt($wachtwoord, $gehashtWachtwoord)</code></h3>';
    var_dump(crypt($wachtwoord, $gehashtWachtwoord));

//    echo '<h3><code>$mijnHashcode</code></h3>';
//    var_dump($mijnHashcode);




//    echo '<h3>verificatie $wachtwoord en $mijnHashcode</h3>';
//
//    var_dump(verifieerWachtwoord($wachtwoord, $mijnHashcode));


//    if (verifieerWachtwoord($wachtwoord, $mijnHashcode)) {
//        echo 'Wachtwoord correct';
//    } else {
//        echo 'Wachtwoord verkeerd';
//    }
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Formulier POST</title>
</head>
<body>
<h1>POST formulier</h1>
<form action="" method="post">
    <div>
        <label>Voornaam:
            <input type="text" name="voornaam">
        </label>
    </div>
    <div>
        <label>Familienaam:
            <input type="text" name="familienaam">
        </label>
    </div>
    <div>
        <label for="mijn-gebruikersnaam">Gebruikersnaam:</label>
        <input id="mijn-gebruikersnaam" type="text" name="gebruikersnaam">
    </div>
    <div>
        <label for="mijn-wachtwoord">Wachtwoord:</label>
        <input id="mijn-wachtwoord" type="password" name="wachtwoord">
    </div>
    <input type="submit" value="Registeren" name="btn-register">
</form>
</body>
</html>