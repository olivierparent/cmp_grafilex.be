<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Oefening PHP</title>
</head>
<body>
<?php

//phpinfo();

echo '<h1>Integer (geheel getal)</h1>';
$i = 1;

echo $i;
// Debugfunctie.
var_dump($i);

$i = $i + 1;
var_dump($i);


// Zelfde als $i = $i + 1;
// Post-increment operator
// Eerst $i tonen en dan pas ++
echo '<h2>Post-increment <code>$i++</code></h2>';
var_dump($i++);
var_dump($i);


// Pre-increment operator
// Eerst ++ uitvoeren dan $i tonen
echo '<h2>Pre-increment <code>++$i</code></h2>';
var_dump(++$i);
var_dump($i);

// Post-decrement operator
// Eerst $i tonen en dan pas --
echo '<h2>Post-decrement <code>$i--</code></h2>';
var_dump($i--);
var_dump($i);

// Pre-decrement operator
// Eerst -- uitvoeren dan $i tonen
echo '<h2>Pre-decrement <code>--$i</code></h2>';
var_dump(--$i);
var_dump($i);

echo '<h1>Float (vlottende kommagetal)</h1>';
$f = 1.0;
var_dump($f);

$f = 1.1;
var_dump($f);

$f = 1.0 + .2;
var_dump($f);

echo '<h1>Boolean (Booleaanse waarde)</h1>';

$b = true;
var_dump($b);

$b = false;
var_dump($b);

echo '<h2>Logische operator EN (<code>&&</code>)</h2>';

echo '<code>true && true</code>';
var_dump(true && true);

echo '<code>false && true</code>';
var_dump(false && true);

echo '<code>true && false</code>';
var_dump(true && false);

echo '<code>false && false</code>';
var_dump(false && false);

echo '<h2>Logische operator OF (<code>||</code>)</h2>';

echo '<code>true || true</code>';
var_dump(true || true);

echo '<code>false || true</code>';
var_dump(false || true);

echo '<code>true || false</code>';
var_dump(true || false);

echo '<code>false || false</code>';
var_dump(false || false);

$b = true;
if ($b) {
    echo 'Boolean is WAAR';
} else {
    echo 'Boolean is ONWAAR';
}

$b = false;
if ($b) {
    echo 'Boolean is WAAR';
} else {
    echo 'Boolean is ONWAAR';
}

echo '<h1>String (tekenstring)</h1>';
$s = 'Hallo wereld';
echo $s;

$s1 = 'Hallo';
$s2 = 'wereld';


echo '<h2>Concatenatie operator (<code>.</code>)</code></h2>';

echo $s1 . ' ' . $s2;

echo '<br>';
$naam = 'Olivier';

echo 'Hallo ' . $naam . '!';

echo '<br>';

// Variabeleninterpolatie:
// variabelen in een string worden
// vervangen door de waarde.
echo "Hallo {$naam}!";

echo '<br>';

echo 'Hallo {$naam}!';

echo '<h1>Array</h1>';

$a = [];
var_dump($a);

$a = ['één', 'twee', 'drie'];
var_dump($a);

foreach ($a as $arrayItem) {
    echo $arrayItem;
}

//for ($i = 0; $i < count($a); $i++) {
//    echo $a[$i];
//}


$a[] = 'vier';
var_dump($a);


$a = [
    1 => 'één',
    2 => 'twee',
    3 => 'drie',
];
var_dump($a);

$frans = [
    'one'   => 'un',
    'two'   => 'deux',
    'three' => 'trois',
];

$nederlands = [
    'one'   => 'één',
    'two'   => 'twee',
    'three' => 'drie',
];

$translation = $frans;
//$translation = $nederlands;

echo $translation['one'];
// Multidimensionale array's (array's in array's)
$translation = [
    'one' => [
        'fr' => 'un',
        'nl' => 'één',
    ],
    'two' => [
        'nl' => 'twee',
        'fr' => 'deux',
    ],
    'three' => [
        'nl' => 'drie',
        'fr' => 'trois',
    ],
];

echo $translation['one']['nl'];
echo $translation['one']['fr'];
?>
</body>
</html>



