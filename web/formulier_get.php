<?php
if (isset($_GET['btn-register'])) {
    var_dump($_GET);
}

?><!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Formulier GET</title>
</head>
<body>
<h1>GET formulier</h1>
<form action="">
    <div>
        <label>Voornaam:
            <input type="text" name="voornaam">
        </label>
    </div>
    <div>
        <label>Familienaam:
            <input type="text" name="familienaam">
        </label>
    </div>
    <div>
        <label for="mijn-gebruikersnaam">Gebruikersnaam:</label>
        <input id="mijn-gebruikersnaam" type="text" name="gebruikersnaam">
    </div>
    <div>
        <label for="mijn-wachtwoord">Wachtwoord:</label>
        <input id="mijn-wachtwoord" type="password" name="wachtwoord">
    </div>
    <input type="submit" value="Registeren" name="btn-register">
</form>
</body>
</html>